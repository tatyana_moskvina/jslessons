var arr1 = ['a', 'b', 'c', 'd','e', 'f', 'g', 'h'];
var arr2 = ['1', '2', '3', '4','5', '6', '7', '8'];

var previousCell = null;
var previousBackgroundColor = null;

var currentCell = null;

function OnTableClickHandler(event)
{
    var cell = event.target;
    var row = cell.parentNode;

    var rowIndex = row.rowIndex;
    var cellIndex = cell.cellIndex;

    if (cellIndex == 0 || rowIndex == 0) return;

    SelectCell(rowIndex, cellIndex);
}

function OnKeyDownHandler(event)
{
    if (currentCell == null) return;

    var cell = currentCell;
    var row = cell.parentNode;

    var rowIndex = row.rowIndex;
    var cellIndex = cell.cellIndex;

    if (event.keyCode == 37 && cellIndex > 1)
    {
        SelectCell(rowIndex, cellIndex - 1);
        return;
    }

    if (event.keyCode == 39 && cellIndex < 8)
    {
        SelectCell(rowIndex, cellIndex + 1);
        return;
    }

    if (event.keyCode == 38 && rowIndex > 1)
    {
        SelectCell(rowIndex - 1, cellIndex);
        return;
    }

    if (event.keyCode == 40 && rowIndex < 8)
    {
        SelectCell(rowIndex + 1, cellIndex);
    }
}

function SelectCell(x, y)
{
    var newElem = document.getElementById("myTable");

    var row = newElem.rows[x];
    var cell = row.cells[y];

    if (previousCell != null) {
        previousCell.style.backgroundColor = previousBackgroundColor;
    }

    previousCell = cell;
    previousBackgroundColor = cell.style.backgroundColor;

    currentCell = cell;

    cell.style.backgroundColor = '#FF1100';

    var char = arr1[y - 1];
    var paragraph = document.getElementById('selectedCell');
    paragraph.innerHTML = char + ':' + (x).toString();
}

function CreateTable2()
{
    var n=8;
    var newElem = document.getElementById("myTable");
    newElem.onclick = OnTableClickHandler;
    newElem.setAttribute('border','1');

    for (var r = 0; r < n+1; r++)
    {
        var newRow = newElem.insertRow(r);

        for (var c = 0; c < n+1; c++)
        {
            var newCell = newRow.insertCell(c);
            if (r==0 && c==0) {
                newCell.height = "50";
                newCell.width = "50";
                newCell.style.backgroundColor = 'yellow';
            }
            else
            if (r==0) {
                var t1=arr1 [c-1];
                newCell.height = "50";
                newCell.width = "100";
                newCell.style.backgroundColor = 'yellow';
                newCell.style.textAlign = 'center';
                newCell.innerHTML = t1;
            }
            else
                if (c==0) {
                    var t2=arr2 [r-1];
                    newCell.height = "100";
                    newCell.width = "50";
                    newCell.style.backgroundColor = 'yellow';
                    newCell.style.textAlign = 'center';
                    newCell.innerHTML = t2;
                }
                else if ((r + c) % 2 != 0) {
                    newCell.height = "100";
                    newCell.width = "100";
                    newCell.style.backgroundColor = 'black';
                    /*newCell.onclick = function (event) {
                        var c1 = arr1 [c - 1];
                        var r1 = arr2 [r - 1];
                        var b = c1 + r1;
                        alert(b);
                    }*/
                } else {
                    newCell.height = "100";
                    newCell.width = "100";
                    newCell.style.backgroundColor = 'white';
                    /*newCell.onclick = function (event) {
                        var c1 = arr1 [c - 1];
                        var r1 = arr2 [r - 1];
                        var b = c1 + r1;
                        alert(b);
                    }*/
                }
        }
    }
    document.body.appendChild(newElem);
}
