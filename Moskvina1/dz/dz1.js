function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

	function RandColor()
{
    var col = Math.round(255.0*Math.random());
    var r = col.toString(16);
    col = Math.round(255.0*Math.random());
    var g=col.toString(16);
    col = Math.round(255.0*Math.random());
    var d=col.toString(16);
    col=r+g+d;
    return col;
}

function CreateTable()
{
    var arr = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];

    var cols = prompt("Введите количество столбцов", "");
    var rows = prompt("Введите количество строк", "");
    cols = parseInt(cols);
    rows = parseInt(rows);
    
    var newElem = document.createElement("table");

    for (var r = 0; r < rows; r++)
    {
        var newRow = newElem.insertRow(r);

        for (var c = 0; c < cols; c++)
        {
            var t = arr[getRandomInt(0, 32)];
            var newCell = newRow.insertCell(c);
            newCell.width = "100";
            newCell.style.backgroundColor = RandColor();
            newCell.style.textAlign = 'center';
            newCell.innerHTML = t;
        }
    }
    document.body.appendChild(newElem);
}
