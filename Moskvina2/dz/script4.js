function ajaxComments () {
    var text = document.getElementById("text").value;
    var request = new XMLHttpRequest ();
    request.open("GET","../ajax.php?input=" + text);
    request.send();

    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200)
        {
            var res = document.getElementById("result");
            res.innerHTML = request.responseText;
        }
    }
}
window.onload = function () {

    var button = document.getElementById("submit");

    button.onclick = function(event)
    {
        ajaxComments();
        event.preventDefault();
    }
};