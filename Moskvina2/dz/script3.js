function MyHtml () {
	var text = '';

	this.addText = function (str) {
		text = text + str;
		return this;
	}

	this.addH = function(str, n)
	{
		text = text + "<h" + n + ">" + str + "</h" + n + ">";
	}

	this.showHTML = function () {
		return text;
	}
}

window.onload = function () {

	var myHtml = new MyHtml();
	myHtml.addText('третья часть');
	myHtml.addH('Title', 1);
	myHtml.addH('Hello', 2);


	document.body.innerHTML = myHtml.showHTML();
};