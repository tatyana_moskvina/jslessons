String.prototype.AddToElement = function (elementName, className) {
	var element = document.createElement(elementName);
	element.className = className;
	element.innerHTML = this;

	document.body.appendChild(element);
}

window.onload = function () {
	var str = 'Hello World';
	str.AddToElement ("div","myDiv");
}